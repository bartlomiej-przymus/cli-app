# CLI-APP

## About
CLI-APP is a small CLI App which purpose is to generate payroll dates
write them to CSV file.

Payroll dates by design are generated for the next 12 months, meaning
if command is run in May the pay dates will start from June and run till
May next year producing 12 rows in file.

The App is configured to generate Basic and Bonus pay date for each month period.

## Installation
1. Clone the repo
2. Run: `composer install`

## Usage
You can run the app by entering the following command in directory where
code is cloned to.
```
php command.php generate-payroll
```
If everything run smoothly, you should see the following prompt in the terminal:
```text
Generating File...
Command run successful!
Please check store directory for updated file
```
The generated file `payroll.csv` should be inside the store folder.

If the command is run multiple times, generated file will be overwritten
on each command run.

## Testing
The App has a basic test suite set up. You can run it with this command:
```
composer run-script tests
```


## Notes
I really enjoyed designing and coding this little project.
I'm looking forward to your feedback.

Areas to improve if more time allowed:
- implementation of better error handling
- better test coverage

Points of potential improvement:
- adding ability to specify which month to start calculation from
- adding support for different file formats
- adding ability to control time format
- adding ability to specify file output path and filename
