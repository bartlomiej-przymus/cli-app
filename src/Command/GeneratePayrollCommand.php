<?php

namespace App\Command;

use App\Services\WritePayrollFile;
use Carbon\Carbon;
use App\Data\PayrollData;
use App\Services\CalculatePaymentDate;
use League\Csv\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePayrollCommand extends Command
{
    protected static $defaultName = 'generate-payroll';

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('This command generates Payroll dates for Basic and Bonus payments for the next 12 months.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $date = Carbon::now()->setDay(1)->addMonth();
        $records = [];

        for ($i = 0; $i < 12; $i++) {
            $payrollData = new PayrollData;
            $payDate = new CalculatePaymentDate;

            $payDate->setYear($date->year)->setPeriod($date->month);

            $payrollData->setPeriodDate($date);

            $payrollData->setBasicPayDate($payDate->calculate());

            $payrollData->setBonusPayDate($payDate->setBonus()->calculate());

            $records[] = $payrollData->getPayrollDatesRecord();

            $date->addMonth();
        }

        $output->writeln('Generating File...');

        try {
            (new WritePayrollFile)->setRecords($records)->writeFile();

            $output->writeln('Command run successful!');
            $output->writeln('Please check storage directory for updated file');

            return Command::SUCCESS;
        } catch (Exception $e) {
            $output->writeln('Ooooops something bad happened check report below!');
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }
    }
}
