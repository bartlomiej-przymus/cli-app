<?php

namespace App\Data;

use Carbon\Carbon;

class PayrollData
{
    public Carbon $periodDate;

    public Carbon $basicPayDate;

    public Carbon $bonusPayDate;

    public function setPeriodDate(Carbon $periodDate): self
    {
        $this->periodDate = $periodDate;

        return $this;
    }

    public function setBasicPayDate(Carbon $basicPayDate): self
    {
        $this->basicPayDate = $basicPayDate;

        return $this;
    }

    public function setBonusPayDate(Carbon $bonusPayDate): self
    {
        $this->bonusPayDate = $bonusPayDate;

        return $this;
    }

    public function getPeriodDate(): string
    {
        return $this->periodDate->format('M/y');
    }

    public function getBasicPayDate(): string
    {
        return $this->basicPayDate->format('Y-m-d');
    }

    public function getBonusPayDate(): string
    {
        return $this->bonusPayDate->format('Y-m-d');
    }

    public function getPayrollDatesRecord(): array
    {
        return [
            $this->getPeriodDate(),
            $this->getBasicPayDate(),
            $this->getBonusPayDate()
        ];
    }
}
