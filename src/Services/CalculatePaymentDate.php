<?php

namespace App\Services;

use Carbon\Carbon;

class CalculatePaymentDate
{
    public int $year;

    public int $period;

    public bool $bonus = false;

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function setPeriod(int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function setBonus(): self
    {
        $this->bonus = true;

        return $this;
    }

    public function calculate(): Carbon
    {
        $period = $this->period;
        $year = $this->year;
        $bonus = $this->bonus;

        if ($bonus) {
            $date = Carbon::createFromDate($year, $period, 10);
            if ($date->dayOfWeek === Carbon::SATURDAY) {
                $date->addDays(2);

                return $date;
            } else if ($date->dayOfWeek === Carbon::SUNDAY) {
                $date->addDay();

                return $date;
            }

                return $date;
        }

        $date = Carbon::createfromDate($year, $period);

        if ($date->lastOfMonth()->dayOfWeek === Carbon::SATURDAY) {
            $date->subDay();

            return $date;
        } else if ($date->lastOfMonth()->dayOfWeek === Carbon::SUNDAY) {
            $date->subDays(2);

            return $date;
        }

        return $date;
    }
}
