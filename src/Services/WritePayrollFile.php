<?php

namespace App\Services;

use League\Csv\Writer;

class WritePayrollFile
{
    private array $headers = [
        'Period',
        'Basic Payment',
        'Bonus Payment'
    ];

    public array $records = [];

    public function setRecords(array $records): self
    {
        $this->records = $records;

        return $this;
    }

    /**
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function writeFile(): void {
        $csv = Writer::createFromPath('store/payroll.csv', 'w+');

        //insert the header
        $csv->insertOne($this->headers);

        //insert all the records
        $csv->insertAll($this->records);
    }
}
