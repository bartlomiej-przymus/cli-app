<?php

namespace Tests\Unit;

use App\Services\CalculatePaymentDate;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class CalculatePaymentDateTest extends TestCase
{
    /** @test */
    public function can_set_year(): void
    {
        $calcDate = new CalculatePaymentDate;

        $calcDate->setYear(2020);

        $this->assertEquals(2020, $calcDate->year);
    }

    /** @test */
    public function can_set_period(): void
    {
        $calcDate = new CalculatePaymentDate;

        $calcDate->setPeriod(2);

        $this->assertEquals(2, $calcDate->period);
    }

    /** @test */
    public function can_set_bonus(): void
    {
        $calcDate = new CalculatePaymentDate;

        $calcDate->setBonus();

        $this->assertTrue($calcDate->bonus);
    }

    /** @test */
    public function can_calculate_basic_pay_date(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate->setYear(2020)->setPeriod(3)->calculate();

        $this->assertInstanceOf(Carbon::class, $payDate);

        $this->assertEquals('2020-03-31', $payDate->toDateString());
    }

    /** @test */
    public function can_calculate_bonus_pay_date(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate
            ->setBonus()
            ->setYear(2020)
            ->setPeriod(3)
            ->calculate();

        $this->assertEquals('2020-03-10', $payDate->toDateString());
    }

    /** @test */
    public function will_bump_basic_pay_date_if_last_day_is_Saturday(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate
            ->setYear(2022)
            ->setPeriod(12)
            ->calculate();
        $this->assertEquals('2022-12-30', $payDate->toDateString());
    }

    /** @test */
    public function will_bump_basic_pay_date_if_last_day_is_Sunday(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate
            ->setYear(2023)
            ->setPeriod(4)
            ->calculate();
        $this->assertEquals('2023-04-28', $payDate->toDateString());
    }

    /** @test */
    public function will_bump_bonus_pay_date_if_10th_day_is_Saturday(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate
            ->setBonus()
            ->setYear(2022)
            ->setPeriod(12)
            ->calculate();
        $this->assertEquals('2022-12-12', $payDate->toDateString());
    }

    /** @test */
    public function will_bump_bonus_pay_date_if_10th_day_is_Sunday(): void
    {
        $calcDate = new CalculatePaymentDate;

        $payDate = $calcDate
            ->setBonus()
            ->setYear(2023)
            ->setPeriod(9)
            ->calculate();
        $this->assertEquals('2023-09-11', $payDate->toDateString());
    }
}
