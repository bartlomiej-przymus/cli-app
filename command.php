<?php

require_once __DIR__.'/vendor/autoload.php';

use App\Command\GeneratePayrollCommand;
use Symfony\Component\Console\Application;

$application = new Application;

$application->add(new GeneratePayrollCommand);

$application->run();
